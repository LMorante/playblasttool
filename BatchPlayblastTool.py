import maya.cmds as cmds
import subprocess

# Not working proprly. UI elements like shotmask dont work in batch?

preset = r"C:\Users\lmorante\Documents\maya\Presets\playblastTest.json"
#file = r"X:\2021\20210419_LIL_XGA\06_shots\000TST\tst0010\anm\maya\versions\XGA_000TST_tst0010_anm_blocking_main_044.ma"
file = r"C:/Users/lmorante/Desktop/Repo/playblasttool/testScene.ma"
currentScene = file.replace("\\","/").rsplit(".",1)[0].rsplit("/",1)[-1]
#outDir = r"C:\Users\lmorante\Desktop\Repo\playblasttool\output"  + r"\{0}".format(currentScene)
outDir = r"C:\Users\lmorante\Desktop\Repo\playblasttool\output"
outFile = "{0}.%4d".format(currentScene)
outFormat = "jpg"
start = 1001
end = 1003
camera = "renderCam"
resolution = [1280,720]

def renderTest():
    cmd = r"C:\Program Files\Autodesk\Maya2020\bin\Render.exe -verb"
    #cmd += " -r hw2".format(preset)
    cmd += " -rd {0} -of {2}".format(outDir,outFile,outFormat)
    cmd += " -s {0} -e {1}".format(start,end)
    cmd += " -cam {0} -x {1} -y {2}".format(camera,resolution[0],resolution[1])
    cmd += " {0}".format(file)
    print (cmd)

    maya = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    out,err = maya.communicate()
    exitcode = maya.returncode
    if str(exitcode) != '0':
        print(err)
        print ('error with file: %s' % (file))
    else:
        print ("Rendering done: {0}".format(outDir))

def renderTestB():
    import maya.standalone
    maya.standalone.initialize("Python")
    cmds.file(file,o=True)
    print (cmds.ogsRender())
    maya.standalone.uninitialize()

if __name__ == "__main__":
    renderTest()