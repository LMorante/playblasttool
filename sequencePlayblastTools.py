# coding=utf-8
# ------------------------------------------------------------------------------------
#DONE filter shots to playblast and playblast only selected shots option.
#DONE Shotmask check sequence and shot data.
#DONE Folder Version handeling. not propper version.
#TODO change pannelSettings on playblast
#TODO GOBLIN template keys error when openning scene from projec X in a maya opened with project Y. Only if the projects dont have the same templates.
# ------------------------------------------------------------------------------------

import os
import sys
import shutil
import platform
import subprocess
import maya.cmds as cmds


def make_dir(path):
    """
    input a path to check if it exists, if not, it creates all the path
    :return: path string
    """
    if not os.path.exists(path):
        os.makedirs(path)
    return path


def openFolder(dir_path):
    ''' Opens in is explorer the specified path '''
    psystem = platform.system()
    if psystem == 'Windows':
        dir_path = dir_path.replace("/", "\\")
        subprocess.call('explorer.exe "%s"' % dir_path)
    else:
        subprocess.call(['open', dir_path])


def clearSelection():
    """
    Maya doesn't replace selection when selecting a shot just adds to the
    current one, this gets just the selected shot
    """
    current = cmds.ls(selection = True, type = "shot")
    cmds.select(clear = True)
    cmds.select(current, add = True)


def getShot():
    """
    Gets a single shot from selection, throws error if more than one or none
    founded
    """
    clearSelection()
    shotSelect = cmds.ls(selection = True)
    return shotSelect


def get_audioNode(name="audio"):
    rawAudioNodes = cmds.ls(type="audio")
    validAudioNodes = []
    for node in rawAudioNodes:
        fileName = cmds.getAttr("{}.filename".format(node))
        if fileName:
            validAudioNodes.append(node)
    if validAudioNodes:
        for node in validAudioNodes:
            if node == name:
                node = node
            else:
                node=validAudioNodes[0]
    else:
        node=""
    return node


def filterShots(shots=[]):
    # If no shots provided, evaluate selected shots.
    if shots == []:
        shots = getShot()
    # If no shots selected, evaluate all shots.
    if len(shots) == 0:
        scene_shots = cmds.ls(type = "shot")
        if len(scene_shots) == 0:
            cmds.error("No shots in scene. Add shots to the cameraSequencer.")
        else:
            shots = scene_shots

    # Filter muted shots
    filtered_shots =[]
    for shot in shots:
        isMuted = cmds.shot(shot,q=True, mute=True)
        if not isMuted:
            filtered_shots.append(shot)

    return filtered_shots


def tempAttributeOverride(obj,overrideDic):
    savedValues = {}
    for key in overrideDic.keys():
        # Store current values
        value = cmds.getAttr("{0}.{1}".format(obj,key))
        type = cmds.getAttr("{0}.{1}".format(obj,key),type=True)
        savedValues[key] = {'value':value, 'type':type}

        # Override values
        overValue = overrideDic[key]['value']
        overType = overrideDic[key]['type']
        if overType == "float3":
            cmds.setAttr("{0}.{1}".format(obj,key), overValue[0][0],overValue[0][1],overValue[0][2], type = 'double3' )
        else:
            cmds.setAttr("{0}.{1}".format(obj,key), overValue )
        #print "changed attribute> %s: %s"% (key,overValue)
        
    yield

    for key in overrideDic.keys():
        value = savedValues[key]['value']
        type = overrideDic[key]['type']
        # Restore values
        if type == "float3":
            cmds.setAttr("{0}.{1}".format(obj,key), value[0][0],value[0][1],value[0][2], type = 'double3' )
        else:
            cmds.setAttr("{0}.{1}".format(obj,key), value )
        #print "Restored attribute> %s: %s"% (key,value)


def buildTemplatePath(goblin, template = 't_subasset_maya_work_scene', subasset = '', valueOverride = {}):
    # get Template by name
    template = goblin.getTemplate(template)
    entity = goblin.env.getStringEnv()["current_entity"]
    parse = goblin.parseFilePath(cmds.file(sceneName = True, query = True), entity_type = entity, dcc = 'maya')

    # Apply overrides
    for key in valueOverride.keys():
        parse[key] = valueOverride[key]

    # build file path with template
    values = {}
    for key in template.keys():
        if parse.has_key(key):
            #print ("Parse has: %s"% key)
            values[key] = parse[key]
        else:
            cmds.warning("Missing key: %s" % key)
    if subasset != '':
        values['subasset'] = subasset
        values['version'] = goblin.getLatestVersion(template, values) + 1

    # Apply overrides
    for key in valueOverride.keys():
        values[key] = valueOverride[key]

    path = goblin.buildFilePath(template, values)
    return path


def copyAndRenameFile(file,newName=""):
    print ("Copy....")
    original = file
    target = newName
    outDir = target.replace("\\","/").rsplit("/",1)[0]
    make_dir(outDir)

    # Rename previous version of the thumbnail
    leftovers = []
    if os.path.exists(target):
        base, ext = os.path.splitext(target)
        bck_name = base + '_bck' + ext
        try:
            os.rename(target, bck_name)
            leftovers.append(bck_name)
        except:
            error = "A file already exists with the same name and it cannot be renamed:\n{}".format(target)
            print(error)
            displayError(error)
            return

    # Copy thumbnail from local to asset path.
    shutil.copyfile(original, target)

    # Remove temp files
    for removefile in leftovers:
        try:
            os.remove(removefile)
        except:
            pass
        
    return target


def playblast(goblin):
    # Filter shots in scene. First try to use selected; If none: filter all shots in the scene.
    shots = filterShots()

    # Pre playblast hook
    sys.path.append(r"G:\04_PIPE\framework\configuration\hooks\modules")
    import previzBuild_def as sm_utils
    reload(sm_utils)

    # Get Goblin data
    entity = goblin.env.getStringEnv()["current_entity"]
    parse = goblin.parseFilePath(cmds.file(sceneName = True, query = True), entity_type = entity, dcc = 'maya')
    scene_version = parse['version']

    # Do playblast
    # Get project Resolution
    maya_settings = goblin.getProjectSection('maya', {})
    playblast_size = maya_settings.get('playblast_size', {})
    width = playblast_size.get('width', None)
    height = playblast_size.get('height', None)
    resolution = [width,height]
    for shot in shots:
        # filter shots
        mute = cmds.shot(shot ,q=True, m=True)
        if mute == True:
            continue
        
        # Get data
        shName = cmds.shot(shot,q=True, shotName = True)
        shSGName = shName.split("_")[1]
        shVersion = shName.split("_")[-1]
        shCam =  cmds.shot(shot,q=True, cc = True)
        shCamShape = cmds.listRelatives(shCam, children=True)[0]
        shStart = cmds.shot(shot,q=True, sst = True)
        shEnd = cmds.shot(shot,q=True, set = True)

        # Make output paths
        filePath = buildTemplatePath(goblin, template = 't_sequence_maya_work_playblast', valueOverride = {'shot':shSGName,'ext':"mov",'version': scene_version})
        editorialFilePath = buildTemplatePath(goblin, template = 't_sequence_maya_work_editorial', valueOverride = {'shot':shSGName,'ext':"mov",'version': 0})
        outDir = filePath.replace("\\","/").rsplit("/",1)[0]
        make_dir(outDir)
        
        # Rename previous version of the thumbnail
        leftovers = []
        if os.path.exists(filePath):
            base, ext = os.path.splitext(filePath)
            bck_name = base + '_bck' + ext
            try:
                os.rename(filePath, bck_name)
                leftovers.append(bck_name)
            except:
                error = "A file already exists with the same name and it cannot be renamed:\n{}".format(filePath)
                print(error)
                displayError(error)
                return


        # Remove temp files
        for removefile in leftovers:
            try:
                os.remove(removefile)
            except:
                pass

        # CameraSettings
        cameraPlayblastConfig = {'displayFilmGate': {'type': u'bool', 'value': False}, 'displayGateMaskColor': {'type': u'float3', 'value': [(0.0, 0.0, 0.0)]}, 'displayFilmOrigin': {'type': u'bool', 'value': False}, 'displayFieldChart': {'type': u'bool', 'value': False}, 'displayGateMaskOpacity': {'type': u'float', 'value': 0.0}, 'displaySafeAction': {'type': u'bool', 'value': False}, 'overscan': {'type': u'double', 'value': 1.0}, 'displayGateMask': {'type': u'bool', 'value': False}, 'displayResolution': {'type': u'bool', 'value': False}, 'displaySafeTitle': {'type': u'bool', 'value': False}, 'displayFilmPivot': {'type': u'bool', 'value': False}}
        for item in tempAttributeOverride(obj=shCam,overrideDic=cameraPlayblastConfig):
            
            sm_utils.createShotMask()
            data = sm_utils.build_shotMask_data(goblin,valueOverride = {'shot':shSGName})
            sm_utils.updateShotMask(data,camera=shCam)
            cmds.playblast(
                filename = filePath,
                startTime = shStart,
                endTime = shEnd,
                widthHeight=resolution,
                format= "qt",
                percent=100,
                quality=100,
                showOrnaments=0,
                offScreen=True,
                forceOverwrite=True,
                sequenceTime=True,
                viewer=False,
                clearCache=1,
                sound=get_audioNode()
                )
            copyAndRenameFile(filePath,editorialFilePath)
    openFolder(outDir)

