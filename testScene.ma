//Maya ASCII 2020 scene
//Name: testScene.ma
//Last modified: Tue, Nov 23, 2021 02:24:07 AM
//Codeset: 1252
requires maya "2020";
requires -nodeType "renderSetup" -nodeType "lightItem" -nodeType "lightEditor" "renderSetup.py" "1.0";
requires -nodeType "zshotmask" "zshotmask.py" "1.0.2";
requires "stereoCamera" "10.0";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" -nodeType "aiSkyDomeLight"
		 -nodeType "aiLightDecay" "mtoa" "4.2.1";
requires "Mayatomr" "2010.0m - 3.7.53.5 ";
requires -nodeType "VRaySettingsNode" -dataType "VRaySunParams" -dataType "vrayFloatVectorData"
		 -dataType "vrayFloatVectorData" -dataType "vrayIntData" "vrayformaya" "5";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2020";
fileInfo "version" "2020";
fileInfo "cutIdentifier" "202011110415-b1e20b88e2";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 19042)\n";
fileInfo "vrayBuild" "5.00.21 848ea8a";
fileInfo "UUID" "CECE0E05-4F31-C6E1-7B48-33AEF543A6F4";
createNode transform -s -n "persp";
	rename -uid "2797AE55-487F-86E7-3419-4CA35512D4CE";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -192.03106326693629 210.69253844683556 598.90359602677722 ;
	setAttr ".r" -type "double3" -14.138352729984314 -1097.3999999996329 -8.3326877580265606e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "25ECAF82-4C90-9300-0571-9BB6131F1BB7";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".ncp" 5;
	setAttr ".fcp" 100000;
	setAttr ".coi" 659.86988646473378;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -0.089605941366299069 2.3496444352613253 0.18903790255874142 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "57D9BC1A-41EB-99D2-92AD-A4BBE4E9CCD4";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1746.4427970334859 1230.3584472570153 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "9BF86F3A-4916-3649-E9BB-46ACE50A93BF";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1515.1704151913495;
	setAttr ".ow" 6883.1112870995239;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" 0 231.27238184213638 1230.3584472570153 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "DC2F20E4-43C0-95AB-A702-1195D9915D51";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.66706645239355766 2.4790752394570905 1401.8340919227276 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "3485C3F6-4799-1EFA-F05F-BEA158999309";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1401.8340919227276;
	setAttr ".ow" 8.4528271355541964;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 0 2.3496444352613253 0 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "752C3EB7-4960-B32F-8B45-F98E66B3031A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 2052.3345550088247 0.88986552959838328 -1.1416683600259763 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "B74C3205-4C77-EB3C-1301-989520F83E46";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 2052.3345550088247;
	setAttr ".ow" 121.20931396242237;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 0 4.9361498653888702 -31.933199882507324 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "previz_GRP";
	rename -uid "2F24D3B8-4AB5-F127-077B-F1888A2E9C60";
	setAttr ".t" -type "double3" -0.85004615783691406 0 0 ;
createNode transform -n "anim_240" -p "previz_GRP";
	rename -uid "6B6736B3-40DF-1BE2-C7C6-E2950E1FB885";
createNode transform -n "CAM_GRP" -p "anim_240";
	rename -uid "5D26B415-4B29-340C-51DD-2B996FCA56CD";
	setAttr ".t" -type "double3" 0 79.921085834503174 481.00286854637989 ;
createNode transform -n "renderCam" -p "CAM_GRP";
	rename -uid "F21CFF21-449D-6CD2-1613-8CB87935A6FE";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rpt" -type "double3" 1.6471191483143555e-14 -1.1829037410180878e-15 -1.5137489359262696e-14 ;
createNode camera -n "renderCamShape" -p "renderCam";
	rename -uid "79CF9DAF-45B5-C36B-A4B7-B7BC398F63B7";
	setAttr -k off ".v";
	setAttr -l on ".hfa";
	setAttr -l on ".vfa";
	setAttr ".ff" 3;
	setAttr ".zom" 1.5466170333382026;
	setAttr -l on ".fl" 50;
	setAttr -l on ".lsr";
	setAttr ".ncp" 1.7316103267669678;
	setAttr ".fcp" 1000000;
	setAttr -l on ".fs";
	setAttr -l on ".fd";
	setAttr -l on ".sa";
	setAttr -l on ".coi" 8098.2512317118999;
	setAttr -l on ".ow" 5830.7408868325683;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -332.97798315859427 1919.9039297422773 3343.1890273529261 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".lls" 10;
	setAttr ".ma" no;
	setAttr ".dcf" yes;
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -n "illu_shotMask" -p "CAM_GRP";
	rename -uid "49D4CF40-4AC7-C2E1-8B44-E382CB9522DE";
	setAttr ".t" -type "double3" -0.13229000770139376 0 -0.011573873939444076 ;
	setAttr ".r" -type "double3" 0 -4.9999991265314003 0 ;
createNode zshotmask -n "illu_shotMask_shape" -p "illu_shotMask";
	rename -uid "28878C30-4178-9AC0-C614-45BCF897A7C4";
	setAttr -k off ".v";
	setAttr ".cam" -type "string" "renderCam";
	setAttr ".tlt" -type "string" "ILLUSORIUM";
	setAttr ".tct" -type "string" "previzAsset_setup";
	setAttr ".trt" -type "string" "12/12/2020";
	setAttr ".blt" -type "string" "(1-240)";
	setAttr ".bct" -type "string" "fL:50.0mm        fov:39.6        fS:5.6        fD:5.0cm        fps:24";
	setAttr ".brt" -type "string" "";
	setAttr ".tp" 45;
	setAttr ".fn" -type "string" "Yu Gothic UI semiLight";
	setAttr ".fa" 0.69999998807907104;
	setAttr ".fs" 0.25;
	setAttr ".tbd" no;
	setAttr ".bbd" no;
	setAttr ".bs" 1.6000000238418579;
createNode transform -n "LGT_directional" -p "CAM_GRP";
	rename -uid "8419857B-40A1-669D-C070-EFB35C559EBB";
	setAttr ".t" -type "double3" 1.6471191483143555e-14 0 100 ;
createNode transform -n "lgt_dome_0" -p "LGT_directional";
	rename -uid "989A26DE-4E1F-A1D1-925C-D2BDE0846F31";
createNode aiSkyDomeLight -n "lgt_dome_0Shape" -p "lgt_dome_0";
	rename -uid "AE6EEFF9-44B4-F32E-12C9-2E83862D8711";
	addAttr -ci true -h true -sn "aal" -ln "attributeAliasList" -dt "attributeAlias";
	setAttr -k off ".v";
	setAttr ".csh" no;
	setAttr ".rcsh" no;
	setAttr ".sc" -type "float3" 0.70099998 0.88785005 1 ;
	setAttr ".gskrd" 0;
	setAttr ".intensity" 0.019999999552965164;
	setAttr ".camera" 0;
	setAttr ".aal" -type "attributeAlias" {"exposure","aiExposure"} ;
createNode transform -n "lgt_directional_0" -p "LGT_directional";
	rename -uid "8CC88F3E-4957-4427-36E9-748D1F8F1EDD";
	setAttr ".r" -type "double3" -15.750284467381887 -11.072026491515372 -54.90717973609059 ;
createNode directionalLight -n "lgt_directional_0Shape" -p "lgt_directional_0";
	rename -uid "5D7B0083-4EC2-5FC0-AF24-28B7E17E999C";
	setAttr -k off ".v";
	setAttr ".cl" -type "float3" 1 0.98884565 0.94200003 ;
	setAttr ".urs" no;
	setAttr ".sc" -type "float3" 0.24137931 0.24137931 0.24137931 ;
	setAttr ".dms" yes;
	setAttr ".fs" 5;
	setAttr ".dr" 8192;
	setAttr ".db" 9.9999997473787516e-06;
	setAttr ".lw" -type "string" "0";
createNode transform -n "pCylinder1";
	rename -uid "24DBFF70-4A3F-FAA3-6B70-078BC5C1B51E";
	setAttr ".t" -type "double3" -31.157956516666559 57.352698720943877 0 ;
	setAttr ".r" -type "double3" 44.297880570116504 10.679720397085134 -22.957531111722123 ;
	setAttr ".s" -type "double3" 25.516050842294508 25.516050842294508 25.516050842294508 ;
createNode mesh -n "pCylinderShape1" -p "pCylinder1";
	rename -uid "BFCD3915-46A8-6C49-BF8B-E297FE9BA7AF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCylinder2";
	rename -uid "849A32A6-4C29-D3F2-24C6-96A4B95A9037";
	setAttr ".t" -type "double3" 1.3786706423303912 86.304782209881324 0 ;
	setAttr ".r" -type "double3" 44.297880570116504 10.679720397085134 -22.957531111722123 ;
	setAttr ".s" -type "double3" 25.516050842294508 25.516050842294508 25.516050842294508 ;
createNode mesh -n "pCylinderShape2" -p "pCylinder2";
	rename -uid "24A2F76F-4060-10BC-EC34-D9AD489C9BFD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 84 ".uvst[0].uvsp[0:83]" -type "float2" 0.64860266 0.10796607
		 0.62640899 0.064408496 0.59184152 0.029841021 0.54828393 0.0076473355 0.5 -7.4505806e-08
		 0.45171607 0.0076473504 0.40815851 0.029841051 0.37359107 0.064408526 0.3513974 0.1079661
		 0.34374997 0.15625 0.3513974 0.2045339 0.37359107 0.24809146 0.40815854 0.28265893
		 0.4517161 0.3048526 0.5 0.3125 0.54828387 0.3048526 0.59184146 0.28265893 0.62640893
		 0.24809146 0.6486026 0.2045339 0.65625 0.15625 0.375 0.3125 0.38749999 0.3125 0.39999998
		 0.3125 0.41249996 0.3125 0.42499995 0.3125 0.43749994 0.3125 0.44999993 0.3125 0.46249992
		 0.3125 0.4749999 0.3125 0.48749989 0.3125 0.49999988 0.3125 0.51249987 0.3125 0.52499986
		 0.3125 0.53749985 0.3125 0.54999983 0.3125 0.56249982 0.3125 0.57499981 0.3125 0.5874998
		 0.3125 0.59999979 0.3125 0.61249977 0.3125 0.62499976 0.3125 0.375 0.68843985 0.38749999
		 0.68843985 0.39999998 0.68843985 0.41249996 0.68843985 0.42499995 0.68843985 0.43749994
		 0.68843985 0.44999993 0.68843985 0.46249992 0.68843985 0.4749999 0.68843985 0.48749989
		 0.68843985 0.49999988 0.68843985 0.51249987 0.68843985 0.52499986 0.68843985 0.53749985
		 0.68843985 0.54999983 0.68843985 0.56249982 0.68843985 0.57499981 0.68843985 0.5874998
		 0.68843985 0.59999979 0.68843985 0.61249977 0.68843985 0.62499976 0.68843985 0.64860266
		 0.79546607 0.62640899 0.75190848 0.59184152 0.71734101 0.54828393 0.69514734 0.5
		 0.68749994 0.45171607 0.69514734 0.40815851 0.71734107 0.37359107 0.75190854 0.3513974
		 0.79546607 0.34374997 0.84375 0.3513974 0.89203393 0.37359107 0.93559146 0.40815854
		 0.97015893 0.4517161 0.9923526 0.5 1 0.54828387 0.9923526 0.59184146 0.97015893 0.62640893
		 0.93559146 0.6486026 0.89203393 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 42 ".vt[0:41]"  0.95105714 -1 -0.30901718 0.80901754 -1 -0.5877856
		 0.5877856 -1 -0.80901748 0.30901715 -1 -0.95105702 0 -1 -1.000000476837 -0.30901715 -1 -0.95105696
		 -0.58778548 -1 -0.8090173 -0.80901724 -1 -0.58778542 -0.95105678 -1 -0.30901706 -1.000000238419 -1 0
		 -0.95105678 -1 0.30901706 -0.80901718 -1 0.58778536 -0.58778536 -1 0.80901712 -0.30901706 -1 0.95105666
		 -2.9802322e-08 -1 1.000000119209 0.30901697 -1 0.9510566 0.58778524 -1 0.80901706
		 0.809017 -1 0.5877853 0.95105654 -1 0.309017 1 -1 0 0.95105714 1 -0.30901718 0.80901754 1 -0.5877856
		 0.5877856 1 -0.80901748 0.30901715 1 -0.95105702 0 1 -1.000000476837 -0.30901715 1 -0.95105696
		 -0.58778548 1 -0.8090173 -0.80901724 1 -0.58778542 -0.95105678 1 -0.30901706 -1.000000238419 1 0
		 -0.95105678 1 0.30901706 -0.80901718 1 0.58778536 -0.58778536 1 0.80901712 -0.30901706 1 0.95105666
		 -2.9802322e-08 1 1.000000119209 0.30901697 1 0.9510566 0.58778524 1 0.80901706 0.809017 1 0.5877853
		 0.95105654 1 0.309017 1 1 0 0 -1 0 0 1 0;
	setAttr -s 100 ".ed[0:99]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 0 0 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0
		 29 30 0 30 31 0 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 20 0
		 0 20 1 1 21 1 2 22 1 3 23 1 4 24 1 5 25 1 6 26 1 7 27 1 8 28 1 9 29 1 10 30 1 11 31 1
		 12 32 1 13 33 1 14 34 1 15 35 1 16 36 1 17 37 1 18 38 1 19 39 1 40 0 1 40 1 1 40 2 1
		 40 3 1 40 4 1 40 5 1 40 6 1 40 7 1 40 8 1 40 9 1 40 10 1 40 11 1 40 12 1 40 13 1
		 40 14 1 40 15 1 40 16 1 40 17 1 40 18 1 40 19 1 20 41 1 21 41 1 22 41 1 23 41 1 24 41 1
		 25 41 1 26 41 1 27 41 1 28 41 1 29 41 1 30 41 1 31 41 1 32 41 1 33 41 1 34 41 1 35 41 1
		 36 41 1 37 41 1 38 41 1 39 41 1;
	setAttr -s 60 -ch 200 ".fc[0:59]" -type "polyFaces" 
		f 4 0 41 -21 -41
		mu 0 4 20 21 42 41
		f 4 1 42 -22 -42
		mu 0 4 21 22 43 42
		f 4 2 43 -23 -43
		mu 0 4 22 23 44 43
		f 4 3 44 -24 -44
		mu 0 4 23 24 45 44
		f 4 4 45 -25 -45
		mu 0 4 24 25 46 45
		f 4 5 46 -26 -46
		mu 0 4 25 26 47 46
		f 4 6 47 -27 -47
		mu 0 4 26 27 48 47
		f 4 7 48 -28 -48
		mu 0 4 27 28 49 48
		f 4 8 49 -29 -49
		mu 0 4 28 29 50 49
		f 4 9 50 -30 -50
		mu 0 4 29 30 51 50
		f 4 10 51 -31 -51
		mu 0 4 30 31 52 51
		f 4 11 52 -32 -52
		mu 0 4 31 32 53 52
		f 4 12 53 -33 -53
		mu 0 4 32 33 54 53
		f 4 13 54 -34 -54
		mu 0 4 33 34 55 54
		f 4 14 55 -35 -55
		mu 0 4 34 35 56 55
		f 4 15 56 -36 -56
		mu 0 4 35 36 57 56
		f 4 16 57 -37 -57
		mu 0 4 36 37 58 57
		f 4 17 58 -38 -58
		mu 0 4 37 38 59 58
		f 4 18 59 -39 -59
		mu 0 4 38 39 60 59
		f 4 19 40 -40 -60
		mu 0 4 39 40 61 60
		f 3 -1 -61 61
		mu 0 3 1 0 82
		f 3 -2 -62 62
		mu 0 3 2 1 82
		f 3 -3 -63 63
		mu 0 3 3 2 82
		f 3 -4 -64 64
		mu 0 3 4 3 82
		f 3 -5 -65 65
		mu 0 3 5 4 82
		f 3 -6 -66 66
		mu 0 3 6 5 82
		f 3 -7 -67 67
		mu 0 3 7 6 82
		f 3 -8 -68 68
		mu 0 3 8 7 82
		f 3 -9 -69 69
		mu 0 3 9 8 82
		f 3 -10 -70 70
		mu 0 3 10 9 82
		f 3 -11 -71 71
		mu 0 3 11 10 82
		f 3 -12 -72 72
		mu 0 3 12 11 82
		f 3 -13 -73 73
		mu 0 3 13 12 82
		f 3 -14 -74 74
		mu 0 3 14 13 82
		f 3 -15 -75 75
		mu 0 3 15 14 82
		f 3 -16 -76 76
		mu 0 3 16 15 82
		f 3 -17 -77 77
		mu 0 3 17 16 82
		f 3 -18 -78 78
		mu 0 3 18 17 82
		f 3 -19 -79 79
		mu 0 3 19 18 82
		f 3 -20 -80 60
		mu 0 3 0 19 82
		f 3 20 81 -81
		mu 0 3 80 79 83
		f 3 21 82 -82
		mu 0 3 79 78 83
		f 3 22 83 -83
		mu 0 3 78 77 83
		f 3 23 84 -84
		mu 0 3 77 76 83
		f 3 24 85 -85
		mu 0 3 76 75 83
		f 3 25 86 -86
		mu 0 3 75 74 83
		f 3 26 87 -87
		mu 0 3 74 73 83
		f 3 27 88 -88
		mu 0 3 73 72 83
		f 3 28 89 -89
		mu 0 3 72 71 83
		f 3 29 90 -90
		mu 0 3 71 70 83
		f 3 30 91 -91
		mu 0 3 70 69 83
		f 3 31 92 -92
		mu 0 3 69 68 83
		f 3 32 93 -93
		mu 0 3 68 67 83
		f 3 33 94 -94
		mu 0 3 67 66 83
		f 3 34 95 -95
		mu 0 3 66 65 83
		f 3 35 96 -96
		mu 0 3 65 64 83
		f 3 36 97 -97
		mu 0 3 64 63 83
		f 3 37 98 -98
		mu 0 3 63 62 83
		f 3 38 99 -99
		mu 0 3 62 81 83
		f 3 39 80 -100
		mu 0 3 81 80 83;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pSphere1";
	rename -uid "D09B76C4-4825-9BF6-7F98-BB8059521D9A";
	setAttr ".t" -type "double3" -32.536627158996929 95.955476706194375 0 ;
	setAttr ".s" -type "double3" 18.03387948133452 18.03387948133452 18.03387948133452 ;
createNode mesh -n "pSphereShape1" -p "pSphere1";
	rename -uid "360EB806-484A-2903-2CBF-9C8E74F02DE9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "EC856DD1-4BD0-87DD-DE83-6EB8B6E23F33";
	setAttr -s 5 ".lnk";
	setAttr -s 5 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "520936DC-4555-0363-63EC-3B8EDE1A4FC7";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "2157DD1D-4D90-33D8-E1DB-B7BE96958055";
createNode displayLayerManager -n "layerManager";
	rename -uid "27839390-43FF-22DA-BD0E-EF88E0C8D1C9";
createNode displayLayer -n "defaultLayer";
	rename -uid "32CD1195-4139-7587-0CBE-E09DD42F8191";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "F1FC6902-4D2F-7995-6626-D6B71063AC52";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "79AD06A0-442A-4960-A3F4-8B88D3AEEC3C";
	setAttr ".g" yes;
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "B0940422-49D5-3F2F-4F22-21A07EA25AFE";
	addAttr -ci true -sn "ARV_options" -ln "ARV_options" -dt "string";
	setAttr ".version" -type "string" "4.0.2";
	setAttr ".ARV_options" -type "string" "Test Resolution=100%;Exposure=0.00;";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "5CDEB2B4-4B9C-7A07-70B7-26AAE86E621C";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "17F04326-41DC-D82B-0FAE-05B87A1C3417";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "C81A4211-4DEF-2850-62E5-99B1A976C13D";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "1C94E50A-4E5A-A4A1-0A16-E7B029724A68";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 240 -ast 1 -aet 240 ";
	setAttr ".st" 6;
createNode animCurveTA -n "anim_125_rotateY";
	rename -uid "206914AB-4D3F-BE99-8F1D-18BCEE642A5B";
	setAttr ".tan" 2;
	setAttr ".wgt" no;
	setAttr -s 13 ".ktv[0:12]"  1 0 21 -30.000000000000004 41 -60.000000000000007
		 61 -90 81 -119.99999999999999 101 -149.99999999999997 121 -180 141 -210.00000000000003
		 161 -239.99999999999997 181 -270 201 -300 221 -330.00000000000006 241 -360;
createNode aiLightDecay -n "temp_aiLightDecay2";
	rename -uid "EFE41817-423E-1323-3476-1BB690F7FA73";
createNode aiLightDecay -n "aiLightDecay2";
	rename -uid "EE3DB26F-4774-6885-33F0-85A9242C2826";
	setAttr ".use_far_atten" yes;
	setAttr ".near_end" 124.26035308837891;
	setAttr ".far_end" 147.92900085449219;
createNode lightEditor -n "lightEditor";
	rename -uid "2510E31A-45B3-3E2C-00B7-D08E100C5945";
createNode lightItem -n "lgt_directional_0Shape__LEItem";
	rename -uid "6528F4BD-4F37-9D98-4FC7-7990A49A8BFE";
createNode renderSetup -n "renderSetup";
	rename -uid "193DE46D-44A0-7BCF-4533-4B97DE7BBE94";
createNode lightItem -n "lgt_dome_0Shape__LEItem";
	rename -uid "E04AA872-4470-D2BD-801B-1E912A4FCC1D";
createNode surfaceShader -n "REF_mat";
	rename -uid "C93929C2-4DFC-ADE9-14D8-52A6ED44F3D9";
	setAttr ".oc" -type "float3" 0.048192773 0.048192773 0.048192773 ;
createNode shadingEngine -n "REF_matSG";
	rename -uid "0217E839-4BB1-04EA-C517-FD98DE4D811C";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
	rename -uid "4D95991C-49AB-B638-FA3E-B79EBDFABA4F";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "1839DC58-4BA2-1F8D-02F1-94A16C95EE33";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -2807.6116213558662 -736.69609554819158 ;
	setAttr ".tgi[0].vh" -type "double2" -1453.8301467233709 28.009765355327708 ;
	setAttr -s 5 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" -1855.7142333984375;
	setAttr ".tgi[0].ni[0].y" 5.7142858505249023;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" -2399.208251953125;
	setAttr ".tgi[0].ni[1].y" -379.55325317382813;
	setAttr ".tgi[0].ni[1].nvs" 1923;
	setAttr ".tgi[0].ni[2].x" -1855.7142333984375;
	setAttr ".tgi[0].ni[2].y" -178.57142639160156;
	setAttr ".tgi[0].ni[2].nvs" 1923;
	setAttr ".tgi[0].ni[3].x" -2396.470703125;
	setAttr ".tgi[0].ni[3].y" 10.084031105041504;
	setAttr ".tgi[0].ni[3].nvs" 1923;
	setAttr ".tgi[0].ni[4].x" -2177.77978515625;
	setAttr ".tgi[0].ni[4].y" -379.55325317382813;
	setAttr ".tgi[0].ni[4].nvs" 1923;
createNode shadingEngine -n "HumanBody_humanBodySG";
	rename -uid "E66DEA20-471B-3654-CF42-4AB89B6D5979";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "HumanBody_materialInfo1";
	rename -uid "47A96E85-4838-9A96-0802-32BD7655B519";
createNode file -n "HumanBody_ReflectionMap";
	rename -uid "87E37DB3-4C65-0CC0-A188-90874F3AB209";
	setAttr ".ftn" -type "string" "C:/Program Files/Autodesk/Mudbox 2014/Textures/Lightprobes/horizon.exr";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "HumanBody_place2dTexture1";
	rename -uid "2FC5CE29-466D-3649-A4BC-B298CF2BFEE6";
createNode blinn -n "HumanBody_blinn1";
	rename -uid "463FFE7A-4FB0-7365-A8FB-33A8590DA292";
	setAttr ".c" -type "float3" 0.92150003 0.68229997 0.53719997 ;
createNode shadingEngine -n "HumanBody_blinn1SG";
	rename -uid "4930FA18-41FD-35F8-B284-E8A59113BD48";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "HumanBody_materialInfo2";
	rename -uid "4333B1F5-42A0-867C-EDD4-78BCB0BAA413";
createNode VRaySettingsNode -s -n "vraySettings";
	rename -uid "137E4C02-4DAC-E90C-2CD6-A4A443A0DBBF";
	setAttr ".gi" 1;
	setAttr ".rfc" 1;
	setAttr ".pe" 2;
	setAttr ".se" 3;
	setAttr ".cmph" 60;
	setAttr ".cfile" -type "string" "";
	setAttr ".cfile2" -type "string" "";
	setAttr ".casf" -type "string" "";
	setAttr ".casf2" -type "string" "";
	setAttr ".st" 3;
	setAttr ".msr" 6;
	setAttr ".aaft" 3;
	setAttr ".aafs" 2;
	setAttr ".dma" 24;
	setAttr ".dam" 1;
	setAttr ".pt" 0.0099999997764825821;
	setAttr ".pmt" 0;
	setAttr ".sd" 1000;
	setAttr ".ss" 0.01;
	setAttr ".pfts" 20;
	setAttr ".ufg" 1;
	setAttr ".fnm" -type "string" "";
	setAttr ".lcfnm" -type "string" "";
	setAttr ".asf" -type "string" "";
	setAttr ".lcasf" -type "string" "";
	setAttr ".urtrshd" 1;
	setAttr ".rtrshd" 2;
	setAttr ".lightCacheType" 1;
	setAttr ".ifile" -type "string" "";
	setAttr ".ifile2" -type "string" "";
	setAttr ".iasf" -type "string" "";
	setAttr ".iasf2" -type "string" "";
	setAttr ".pmfile" -type "string" "";
	setAttr ".pmfile2" -type "string" "";
	setAttr ".pmasf" -type "string" "";
	setAttr ".pmasf2" -type "string" "";
	setAttr ".dmcstd" 1;
	setAttr ".dmculs" 0;
	setAttr ".dmcsat" 0.004999999888241291;
	setAttr ".cmtp" 6;
	setAttr ".cmao" 2;
	setAttr ".cg" 2.2000000476837158;
	setAttr ".mtah" 1;
	setAttr ".rgbcs" -1;
	setAttr ".suv" 0;
	setAttr ".srflc" 1;
	setAttr ".srdml" 0;
	setAttr ".seu" 1;
	setAttr ".gormio" 1;
	setAttr ".gocle" 1;
	setAttr ".gopl" 2;
	setAttr ".wi" 960;
	setAttr ".he" 540;
	setAttr ".aspr" 1.7777780294418335;
	setAttr ".productionGPUResizeTextures" 0;
	setAttr ".autolt" 0;
	setAttr ".jpegq" 100;
	setAttr ".animtp" 1;
	setAttr ".vfbOn" 1;
	setAttr ".vfbSA" -type "Int32Array" 847 124 20 0 0 0 0
		 0 0 0 0 -1073724351 0 0 0 0 0 0 0
		 0 0 0 -1074790400 0 -1074790400 0 -1074790400 0 -1074790400 1 0
		 0 0 3254 1 3242 0 1 3234 1700143739 1869181810 825893486 1632379436
		 1936876921 578501154 1936876886 577662825 573321530 1935764579 574235251 1953460082 1881287714 1701867378 1701409906 2067407475
		 1919252002 1852795251 741423650 1835101730 574235237 1696738338 1818386798 1949966949 744846706 1886938402 577007201 1818322490
		 573334899 1634760805 1650549870 975332716 1702195828 1931619453 1814913653 1919252833 1530536563 1818436219 577991521 1751327290
		 779317089 1886611812 1132028268 1701999215 1869182051 573317742 1886351984 1769239141 975336293 1702240891 1869181810 825893486
		 1634607660 975332717 1936278562 2036427888 1919894304 1952671090 577662825 1852121644 1701601889 1920219682 573334901 1634760805
		 975332462 1702195828 2019893804 1684955504 1701601889 1920219682 573334901 1718579824 577072233 573321530 1869641829 1701999987
		 774912546 1763847216 1717527395 577072233 740434490 1667459362 1852142175 1953392996 578055781 573321274 1886088290 1852793716
		 1715085942 1702063201 1668227628 1717530473 577072233 740434490 1768124194 1868783471 1936879468 1701011824 741358114 1768124194
		 1768185711 1634496627 1986356345 577069929 573321274 1869177711 1701410399 1634890871 1868985198 975334770 1864510512 1601136995
		 1702257011 1835626089 577070945 1818322490 746415475 1651864354 2036427821 577991269 578509626 1935764579 574235251 1868654691
		 1701981811 1869819494 1701016181 1684828006 740455013 1869770786 1953654128 577987945 1981971258 1769173605 975335023 1847733297
		 577072481 1867719226 1701016181 1196564538 573317698 1650552421 975332716 1702195828 2019893804 1684955504 1634089506 744846188
		 1886938402 1633971809 577072226 1818322490 573334899 1667330159 578385001 808333626 1818370604 1600417381 1701080941 741358114
		 1668444962 1887007839 809116261 1931619453 1814913653 1919252833 1530536563 1818436219 577991521 1751327290 779317089 778462578
		 1751607660 2020175220 1881287714 1701867378 1701409906 2067407475 1919252002 1852795251 741423650 1835101730 574235237 1751607628
		 2020167028 1696738338 1818386798 1715085925 1702063201 2019893804 1684955504 1634089506 744846188 1886938402 1633971809 577072226
		 1970435130 573341029 761427315 1702453612 975336306 746413403 1818436219 577991521 1751327290 779317089 778462578 1886220131
		 1953067887 573317733 1886351984 1769239141 975336293 1702240891 1869181810 825893486 1634607660 975332717 1836008226 1769172848
		 740451700 1634624802 577072226 1818322490 573334899 1634760805 975332462 1936482662 1696738405 1851879544 1818386788 1949966949
		 744846706 1634758434 2037672291 774978082 1646406704 1684956524 1685024095 809116261 1931619453 1814913653 1919252833 1530536563
		 2103278941 1663204140 1936941420 1663187490 1936679272 778399790 1869505892 1919251305 1881287714 1701867378 1701409906 2067407475
		 1919252002 1852795251 741423650 1835101730 574235237 1869505860 1919251305 1853169722 1767994977 1818386796 573317733 1650552421
		 975332716 1936482662 1696738405 1851879544 1715085924 1702063201 2019893804 1684955504 1701601889 1920219682 573334901 1667330159
		 578385001 808333626 1818370604 1600417381 1701080941 741358114 1952669986 577074793 1818322490 573334899 1936028272 975336549
		 1931619378 1852142196 577270887 808333626 1634869804 1937074532 808532514 573321262 1665234792 1701602659 1702125938 1920219682
		 573334901 1869505892 1919251305 1685024095 825893477 1931619453 1814913653 1919252833 1530536563 2066513245 1634493218 975336307
		 1634231074 1882092399 1701588581 2019980142 1881287714 1701867378 1701409906 2067407475 1919252002 1852795251 741423650 1835101730
		 574235237 1936614732 1717978400 1937007461 1696738338 1818386798 1715085925 1702063201 2019893804 1684955504 1634089506 744846188
		 1886938402 1633971809 577072226 1970435130 1864510565 1768120688 975337844 741355057 1701601826 1834968174 577070191 573321786
		 1918987367 1852792677 1634089506 744846188 1634494242 1935631730 577075817 774910778 1730292784 1701994860 1768257375 578054247
		 808333626 1818370604 1601007471 1734960503 975336552 808726064 808464432 959787056 1730292790 1701994860 1919448159 1869116261
		 975332460 741355057 1818846754 1601332596 1635020658 1852795252 774912546 1931619376 1920300129 1869182049 825893486 573321262
		 1685217640 1701994871 1667457375 1919249509 1684370529 1920219682 573334901 1684828003 1918990175 1715085933 1702063201 1852383788
		 1634887028 1986622563 1949966949 744846706 1634624802 1600482402 1684106338 975336293 1702195828 1769153068 577987940 573322810
		 1684106338 1918858085 1952543855 577662825 775237946 1931619376 1634038388 1818386283 975336053 808594992 808464432 959590448
		 1965173816 1734305139 1769234802 975333230 1936482662 1730292837 1769234802 1683973998 1769172581 975337844 808333365 1919361580
		 1852404833 1701601127 1752459118 808532514 573321262 1952543335 1600613993 1836019578 775240226 1730292784 1769234802 1935632238
		 1701867372 774912546 1730292784 1769234802 1935632238 1852142196 577270887 808333626 1937056300 1668243301 1937075299 577662825
		 1818322490 573334899 1818452847 1869181813 2037604206 1952804205 576940402 1970435130 1864510565 1970037603 1852795251 1919250527
		 1953391971 808598050 573321262 1818452847 1869181813 1869766510 1769234804 975335023 741355056 1667460898 1769174380 1633644143
		 975332210 774910001 1965173808 1935631731 1952543331 975333475 1936482662 1931619429 1935635043 1701670265 1667854964 1920219682
		 573334901 1601332083 1953784176 577663589 573321274 1601332083 1953265005 1634494313 1667196274 1953396079 741423650 1919120162
		 1852138591 2037672307 808794658 573321262 1601332083 1735288172 975333492 808333365 1668489772 1819500402 1600483439 1769103734
		 1701015137 774912546 1931619376 2002743907 1752458345 1918989919 1668178281 809116261 573321262 1601332083 1684366707 741358114
		 1919120162 1869576799 842670701 573321262 1601332083 1635020658 1852795252 774912546 1931619376 1935635043 1852142196 577270887
		 808333626 1937056300 1969512293 975336563 1936482662 1679961189 1601467253 1953784176 577663589 573321274 1953723748 1852138591
		 2037672307 808794658 573321262 1953723748 1684107871 1601402217 1769103734 1701015137 774912546 1679961136 1601467253 1953786218
		 975336037 741355056 1937073186 1870290804 975334767 741355058 1937073186 1869766516 1769234804 975335023 741355056 1937073186
		 1953718132 1735288178 975333492 741355057 1634494242 1969186162 1868522867 1635021666 1600482403 1734438249 1715085925 1702063201
		 1818698284 1600483937 1953718895 1701602145 1634560351 1885300071 577270881 740434490 1935830818 1835622260 1600481121 1836019578
		 774978082 1864510512 1601467234 1734438249 1869766501 1769234804 975335023 741355056 1935830818 1835622260 1600481121 1701999731
		 1752459118 774978082 1965173808 1717527923 1702128745 1835622258 577070945 1818322490 573334899 1918987367 1835622245 1600481121
		 1752457584 572668450 1931619453 1814913653 1919252833 1530536563 2103278941 573341021 1768383826 1699180143 2067407470 1919252002
		 1852795251 741423650 1970236706 1717527923 1869376623 1852137335 1701601889 1715085924 1702063201 1869423148 1600484213 1819045734
		 1885304687 1953393007 1668246623 577004907 1818322490 573334899 1937076077 1868980069 2003790956 1768910943 2019521646 741358114
		 1970236706 1717527923 1869376623 1869635447 1601465961 809116281 1377971325 1701080677 1701402226 2067407479 1919252002 1852795251
		 741423650 1634624802 1600482402 1684956530 1918857829 1869178725 1715085934 1702063201 1701978668 1919247470 1734701663 1601073001
		 975319160 808333613 1701978668 1919247470 1734701663 1601073001 975319161 808333613 1701978668 1919247470 1734701663 1601073001
		 975319416 808333613 1701978668 1919247470 1734701663 1601073001 975319417 808333613 1769349676 1918859109 975332453 1702195828
		 1769349676 1734309733 1852138866 1920219682 573334901 2003134838 1970037343 1949966949 744846706 1701410338 1869438839 975335278
		 1936482662 1663183973 1919904879 1634493279 1834971245 577070191 741946938 1819239202 1667199599 1886216556 577662815 1970435130
		 1881287781 1818589289 1718511967 1869373295 1684368227 1634089506 744846188 2020175906 1767861349 1601136238 1920102243 1702126437
		 1868783460 1936879468 1634089506 2103800684 1665212972 1852795252 2067407475 1919252002 1852795251 741423650 1969319970 1346987379
		 1751342930 1701536613 1715085924 1702063201 1937056300 2020167781 1933667429 1952671088 1701339999 1684368227 1634089506 744846188
		 1936028706 1936020084 1684107871 975335273 573321525 1953719668 1601398098 1667590243 577004907 1818322490 573334899 1969382756
		 1634227047 1735289188 1684107871 975335273 573322805 1969382756 1634227047 1735289188 1701339999 1684368227 1634089506 744846188
		 1702130466 1299146098 1600480367 1768186226 926556783 1931619384 1701995892 1685015919 1751342949 1701536613 1715085924 1702063201
		 32125 ;
	setAttr ".mSceneName" -type "string" "L:/20_STUFF/lucas/_Resources/_Script/PrevisSetup/previz_asset_001/previzAsset_blankScene.ma";
	setAttr ".rt_cpuRayBundleSize" 4;
	setAttr ".rt_gpuRayBundleSize" 128;
	setAttr ".rt_maxPaths" 10000;
	setAttr ".rt_engineType" 3;
	setAttr ".rt_gpuResizeTextures" 0;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "FAFEBFDA-422E-C44F-DE2B-B19F08941F16";
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polySphere -n "polySphere1";
	rename -uid "A67AB7ED-4917-AC0A-816C-96A8EE68C24A";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 0 1 1 0 0
		 0 0 1 1 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".hwi" yes;
	setAttr ".aofr" 32;
	setAttr ".aosm" 32;
	setAttr ".adof" no;
	setAttr ".msaa" yes;
	setAttr ".aasc" 16;
	setAttr ".laa" yes;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 5 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 7 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 3 ".u";
select -ne :defaultRenderingList1;
select -ne :lightList1;
	setAttr -s 2 ".l";
select -ne :defaultTextureList1;
select -ne :lambert1;
	setAttr ".c" -type "float3" 0.80232561 0.80232561 0.80232561 ;
select -ne :initialShadingGroup;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".ren" -type "string" "mayaHardware2";
	setAttr ".an" yes;
	setAttr ".ef" 240;
	setAttr ".pff" yes;
	setAttr ".ifp" -type "string" "<Scene>/<Scene>";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr ".w" 1920;
	setAttr ".h" 1080;
	setAttr ".pa" 1;
	setAttr ".dar" 1.7777777910232544;
select -ne :defaultLightSet;
	setAttr -s 2 ".dsm";
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "G:/04_PIPE/OCIO_github_repository/OpenColorIO-Configs/aces_1.2/config.ocio";
	setAttr ".vtn" -type "string" "sRGB (ACES)";
	setAttr ".wsn" -type "string" "ACES - ACEScg";
	setAttr ".ote" yes;
	setAttr ".otn" -type "string" "sRGB (ACES)";
	setAttr ".potn" -type "string" "sRGB (ACES)";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "anim_125_rotateY.o" "anim_240.ry";
connectAttr "lgt_dome_0Shape__LEItem.en" "lgt_dome_0Shape.v";
connectAttr "lgt_directional_0Shape__LEItem.en" "lgt_directional_0Shape.v";
connectAttr "polyCylinder1.out" "pCylinderShape1.i";
connectAttr "polySphere1.out" "pSphereShape1.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "HumanBody_humanBodySG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "REF_matSG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "HumanBody_blinn1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "HumanBody_humanBodySG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "REF_matSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "HumanBody_blinn1SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":defaultArnoldDisplayDriver.msg" ":defaultArnoldRenderOptions.drivers"
		 -na;
connectAttr ":defaultArnoldFilter.msg" ":defaultArnoldRenderOptions.filt";
connectAttr ":defaultArnoldDriver.msg" ":defaultArnoldRenderOptions.drvr";
connectAttr "lgt_directional_0Shape__LEItem.msg" "lightEditor.fi";
connectAttr "lgt_dome_0Shape__LEItem.msg" "lightEditor.li";
connectAttr "lgt_directional_0Shape.msg" "lgt_directional_0Shape__LEItem.lgt";
connectAttr "lightEditor.lit" "lgt_directional_0Shape__LEItem.pls";
connectAttr "lightEditor.en" "lgt_directional_0Shape__LEItem.pen";
connectAttr "lightEditor.nic" "lgt_directional_0Shape__LEItem.pic";
connectAttr "lgt_dome_0Shape.msg" "lgt_dome_0Shape__LEItem.lgt";
connectAttr "lgt_directional_0Shape__LEItem.nxt" "lgt_dome_0Shape__LEItem.prv";
connectAttr "lightEditor.lit" "lgt_dome_0Shape__LEItem.pls";
connectAttr "lightEditor.en" "lgt_dome_0Shape__LEItem.pen";
connectAttr "lightEditor.nic" "lgt_dome_0Shape__LEItem.pic";
connectAttr "REF_mat.oc" "REF_matSG.ss";
connectAttr "REF_matSG.msg" "materialInfo2.sg";
connectAttr "REF_mat.msg" "materialInfo2.m";
connectAttr "REF_mat.msg" "materialInfo2.t" -na;
connectAttr ":initialShadingGroup.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "REF_mat.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr ":initialParticleSE.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr ":lambert1.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[3].dn"
		;
connectAttr "REF_matSG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[4].dn"
		;
connectAttr "HumanBody_humanBodySG.msg" "HumanBody_materialInfo1.sg";
connectAttr "HumanBody_place2dTexture1.o" "HumanBody_ReflectionMap.uv";
connectAttr "HumanBody_place2dTexture1.ofu" "HumanBody_ReflectionMap.ofu";
connectAttr "HumanBody_place2dTexture1.ofv" "HumanBody_ReflectionMap.ofv";
connectAttr "HumanBody_place2dTexture1.rf" "HumanBody_ReflectionMap.rf";
connectAttr "HumanBody_place2dTexture1.reu" "HumanBody_ReflectionMap.reu";
connectAttr "HumanBody_place2dTexture1.rev" "HumanBody_ReflectionMap.rev";
connectAttr "HumanBody_place2dTexture1.vt1" "HumanBody_ReflectionMap.vt1";
connectAttr "HumanBody_place2dTexture1.vt2" "HumanBody_ReflectionMap.vt2";
connectAttr "HumanBody_place2dTexture1.vt3" "HumanBody_ReflectionMap.vt3";
connectAttr "HumanBody_place2dTexture1.vc1" "HumanBody_ReflectionMap.vc1";
connectAttr "HumanBody_place2dTexture1.ofs" "HumanBody_ReflectionMap.fs";
connectAttr ":defaultColorMgtGlobals.cme" "HumanBody_ReflectionMap.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "HumanBody_ReflectionMap.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "HumanBody_ReflectionMap.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "HumanBody_ReflectionMap.ws";
connectAttr "HumanBody_blinn1.oc" "HumanBody_blinn1SG.ss";
connectAttr "HumanBody_blinn1SG.msg" "HumanBody_materialInfo2.sg";
connectAttr "HumanBody_blinn1.msg" "HumanBody_materialInfo2.m";
connectAttr "REF_matSG.pa" ":renderPartition.st" -na;
connectAttr "HumanBody_humanBodySG.pa" ":renderPartition.st" -na;
connectAttr "HumanBody_blinn1SG.pa" ":renderPartition.st" -na;
connectAttr "REF_mat.msg" ":defaultShaderList1.s" -na;
connectAttr "HumanBody_blinn1.msg" ":defaultShaderList1.s" -na;
connectAttr "temp_aiLightDecay2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "aiLightDecay2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "HumanBody_place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "lgt_directional_0Shape.ltd" ":lightList1.l" -na;
connectAttr "lgt_dome_0Shape.ltd" ":lightList1.l" -na;
connectAttr "HumanBody_ReflectionMap.msg" ":defaultTextureList1.tx" -na;
connectAttr "pCylinderShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pSphereShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "lgt_directional_0.iog" ":defaultLightSet.dsm" -na;
connectAttr "lgt_dome_0.iog" ":defaultLightSet.dsm" -na;
// End of testScene.ma
