import maya.cmds as cmds
import maya.mel as mel
import maya.standalone
import subprocess
import os

ROOT = os.path.dirname(os.path.abspath(__file__))
CAMERA = "renderCam"
RESOLUTION = [1280,536]
RENDERER = r"C:\Program Files\Autodesk\Maya2020\bin\Render.exe"
OUTPUT_PATH = ROOT + r"\DemoFiles\output"
RENDER_SCENE = ROOT + r"\DemoFiles\renderScene_002.ma"
RENDER_PRESSET = ROOT + r"\maya\Presets\renderSettings_playblastC.json"
OUTPUT_VIDEO_A = OUTPUT_PATH + "/sceneName_version.mp4"
OUTPUT_VIDEO_B = OUTPUT_PATH + "/editorial.mp4"

def renderTestA():
    # -Initialize Maya
    #maya.standalone.initialize("Python")

    # -Open Scene
    #cmds.file(RENDER_SCENE,o=True)

    # -Change Renderer and settings
    #cmds.setAttr(CAMERA +".overscan",1.0)
    """
    cmds.setAttr("defaultRenderGlobals.currentRenderer", "mayaHardware2", type="string")
    cmds.setAttr('defaultRenderGlobals.ren', 'mayaHardware2', type='string')
    mel.eval('loadPreferredRenderGlobalsPreset("mayaHardware2")')
    mel.eval('setCurrentRenderer "mayaHardware2"')
    cmds.setAttr("hardwareRenderingGlobals.renderMode", 2)
    """
    # -Prepare Commands
    preRenderCmd = 'setAttr "renderCam.overscan" 1.5;'
    cmd = '"{0}"'.format(RENDERER)
    cmd += ' -r hw2'
    cmd += ' -rendersettingspreset "{0}"'.format(RENDER_PRESSET)
    cmd += ' -preRender "{0}"'.format(preRenderCmd)
    cmd += ' -rd {0}'.format(OUTPUT_PATH)
    cmd += ' -s {0} -e {1}'.format(1,1)      #TODO By dafault use scene data. Best to get from SG.
    #cmd += ' -cam {0} -x {1} -y {2}'.format(CAMERA,RESOLUTION[0],RESOLUTION[1])
    cmd += ' -cam {0}'.format(CAMERA)
    cmd += ' "{0}"'.format(RENDER_SCENE)

    # -Make Directories    OUTPUT_PATH  
    # -Do render
    print ("RESULT:")
    print (cmd)

    maya = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    out,err = maya.communicate()
    exitcode = maya.returncode
    if str(exitcode) != '0':
        print(err)
        print ('error with file: %s' % (RENDER_SCENE))
    else:
        print ("Rendering done: {0}".format(OUTPUT_PATH))

    # -Convert ImageSequence to Video.
    # -Save in desired paths
    # -CleanUp and Uninitialize
    #maya.standalone.uninitialize()

#GOOD cmd COMMAND
print ("REFERENCE:")
cmdREF = r'"C:\Program Files\Autodesk\Maya2022\bin\Render.exe" -r hw2 -cam renderCam -s 1 -e 1 -pythonver 3 -preRender "setAttr "renderCam.overscan" 1.0;" -rendersettingspreset "C:\Users\LucasMorante\Documents\maya\Presets\renderSettings_playblastA.json" C:\Users\LucasMorante\Desktop\_Repos\playblasttool\DemoFiles\renderScene_001.ma'
print (cmdREF)


# Final
def runTest():
    renderTestA()
    pass

if __name__ == "__main__":
    runTest()